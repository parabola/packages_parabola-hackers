// Copyright 2015-2016 Luke Shumaker <git.lukeshu@sbcglobal>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"context"

	p "git.lukeshu.com/go/libnslcd/nslcd_proto"
	"git.lukeshu.com/go/libnslcd/nslcd_server"
)

func (o *Hackers) Shadow_ByName(ctx context.Context, req p.Request_Shadow_ByName) <-chan p.Shadow {
	o.lock.RLock()
	ret := make(chan p.Shadow)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		cred, ok := nslcd_server.PeerCredFromContext(ctx)
		if !ok {
			return
		}

		if cred.Uid != 0 {
			return
		}
		uid := o.name2uid(req.Name)
		user := o.users[uid]
		ret <- p.Shadow{
			Name:           user.Passwd.Name,
			PwHash:         user.Passwd.PwHash,
			LastChangeDate: -1,
			MinDays:        -1,
			MaxDays:        -1,
			WarnDays:       -1,
			InactDays:      -1,
			ExpireDate:     -1,
			Flag:           -1,
		}
	}()
	return ret
}

func (o *Hackers) Shadow_All(ctx context.Context, req p.Request_Shadow_All) <-chan p.Shadow {
	o.lock.RLock()
	ret := make(chan p.Shadow)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		cred, ok := nslcd_server.PeerCredFromContext(ctx)
		if !ok {
			return
		}

		if cred.Uid != 0 {
			return
		}

		for _, user := range o.users {
			ret <- p.Shadow{
				Name:           user.Passwd.Name,
				PwHash:         user.Passwd.PwHash,
				LastChangeDate: -1,
				MinDays:        -1,
				MaxDays:        -1,
				WarnDays:       -1,
				InactDays:      -1,
				ExpireDate:     -1,
				Flag:           -1,
			}
		}
	}()
	return ret
}
