// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"context"

	p "git.lukeshu.com/go/libnslcd/nslcd_proto"
)

/* Note that the output password hash value should be one of:
   <empty> - no password set, allow login without password
   !       - used to prevent logins
   x       - "valid" encrypted password that does not match any valid
             password often used to indicate that the password is
             defined elsewhere (i.e., in the shadow database)
   other   - encrypted password, in crypt(3) format

   A "!" prefix on a password hash "locks" the account; therefore a a
   hash of "!" says "no login"; while a hash of "x" says "you may log
   in", but fails to authorize; passing the buck to the next database.
*/

func (o *Hackers) Passwd_ByName(ctx context.Context, req p.Request_Passwd_ByName) <-chan p.Passwd {
	o.lock.RLock()
	ret := make(chan p.Passwd)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		uid := o.name2uid(req.Name)
		if uid < 0 {
			return
		}
		passwd := o.users[uid].Passwd
		passwd.PwHash = "x" // only put actual hashes in the Shadow DB
		ret <- passwd
	}()
	return ret
}

func (o *Hackers) Passwd_ByUID(ctx context.Context, req p.Request_Passwd_ByUID) <-chan p.Passwd {
	o.lock.RLock()
	ret := make(chan p.Passwd)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		user, found := o.users[req.UID]
		if !found {
			return
		}
		passwd := user.Passwd
		passwd.PwHash = "x" // only put actual hashes in the Shadow DB
		ret <- passwd
	}()
	return ret
}

func (o *Hackers) Passwd_All(ctx context.Context, req p.Request_Passwd_All) <-chan p.Passwd {
	o.lock.RLock()
	ret := make(chan p.Passwd)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		for _, user := range o.users {
			passwd := user.Passwd
			passwd.PwHash = "x" // only put actual hashes in the Shadow DB
			ret <- passwd
		}
	}()
	return ret
}
