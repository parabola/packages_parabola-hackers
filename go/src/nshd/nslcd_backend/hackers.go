// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

// Package nslcd_backend is an nslcd_server Backend that
// speaks to hackers.git.
package nslcd_backend

import (
	"fmt"
	"sync"

	"nshd/nshd_files"

	"git.lukeshu.com/go/libnslcd/nslcd_server"
	"git.lukeshu.com/go/libnslcd/nslcd_systemd"
	"git.lukeshu.com/go/libsystemd/sd_daemon"
)

type config struct {
	Pam_password_prohibit_message string
}

type Hackers struct {
	nslcd_server.NilBackend
	lock sync.RWMutex

	CfgFilename string

	cfg    config
	users  map[int32]nshd_files.User
	groups map[string]map[string]bool
}

var _ nslcd_systemd.Backend = &Hackers{}
var _ nslcd_server.Backend = &Hackers{}

func (o *Hackers) Init() error {
	sd_daemon.Log.Debug(fmt.Sprintf("hackers.git: CfgFilename = %v", o.CfgFilename))
	err := o.Reload()
	if err != nil {
		sd_daemon.Log.Err(fmt.Sprintf("hackers.git: Could not initialize: %v", err))
		return err
	}
	return nil
}

func (o *Hackers) Close() {
	sd_daemon.Log.Info("hackers.git: Closing session")
	o.lock.Lock()
	defer o.lock.Unlock()

	o.users = make(map[int32]nshd_files.User, 0)
	o.groups = make(map[string]map[string]bool)
}

func (o *Hackers) Reload() error {
	sd_daemon.Log.Info("hackers.git: Loading session")
	o.lock.Lock()
	defer o.lock.Unlock()

	var err error
	o.cfg, err = parse_config(o.CfgFilename)
	if err != nil {
		return err
	}
	sd_daemon.Log.Info(fmt.Sprintf("hackers.git: pam_password_prohibit_message: %#v", o.cfg.Pam_password_prohibit_message))

	sd_daemon.Log.Debug("hackers.git: Parsing user data")
	o.users, err = nshd_files.LoadAllUsers()
	if err != nil {
		return err
	}

	passwords, err := nshd_files.LoadAllPasswords()
	if err != nil {
		return err
	}

	o.groups = make(map[string]map[string]bool)
	for uid, user := range o.users {
		user.Passwd.GID = usersGid
		hash, hasHash := passwords[user.Passwd.Name]
		if !hasHash {
			hash = "!"
		}
		user.Passwd.PwHash = hash
		o.users[uid] = user
		for _, groupname := range user.Groups {
			o.add_user_to_group(user.Passwd.Name, groupname)
		}
	}
	return nil
}

func (o *Hackers) name2uid(name string) int32 {
	for uid, data := range o.users {
		if data.Passwd.Name == name {
			return uid
		}
	}
	return -1
}

func (o *Hackers) add_user_to_group(username string, groupname string) {
	group, found := o.groups[groupname]
	if !found {
		group = make(map[string]bool)
		o.groups[groupname] = group
	}
	group[username] = true
}
