// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"context"

	"nshd/util"

	p "git.lukeshu.com/go/libnslcd/nslcd_proto"
)

func (o *Hackers) groupByName(name string, users bool) p.Group {
	members_set, found := o.groups[name]
	if !found {
		return p.Group{ID: -1}
	}
	gid := name2gid(name)
	if gid < 0 {
		return p.Group{ID: -1}
	}
	var members_list []string
	if users {
		members_list = util.Set2list(members_set)
	} else {
		members_list = make([]string, 0)
	}
	return p.Group{
		Name:    name,
		PwHash:  "x",
		ID:      gid,
		Members: members_list,
	}
}

func (o *Hackers) groupByGid(gid int32, users bool) p.Group {
	name, found := gid2name(gid)
	if !found {
		return p.Group{ID: -1}
	}
	members_set, found := o.groups[name]
	if !found {
		return p.Group{ID: -1}
	}
	var members_list []string
	if users {
		members_list = util.Set2list(members_set)
	} else {
		members_list = make([]string, 0)
	}
	return p.Group{
		Name:    name,
		PwHash:  "x",
		ID:      gid,
		Members: members_list,
	}
}

func (o *Hackers) Group_ByName(ctx context.Context, req p.Request_Group_ByName) <-chan p.Group {
	o.lock.RLock()
	ret := make(chan p.Group)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		group := o.groupByName(req.Name, true)
		if group.ID < 0 {
			return
		}
		ret <- group
	}()
	return ret
}

func (o *Hackers) Group_ByGid(ctx context.Context, req p.Request_Group_ByGid) <-chan p.Group {
	o.lock.RLock()
	ret := make(chan p.Group)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		group := o.groupByGid(req.Gid, true)
		if group.ID < 0 {
			return
		}
		ret <- group
	}()
	return ret
}

// note that the BYMEMBER call returns an empty members list
func (o *Hackers) Group_ByMember(ctx context.Context, req p.Request_Group_ByMember) <-chan p.Group {
	o.lock.RLock()
	ret := make(chan p.Group)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		uid := o.name2uid(req.Member)
		if uid < 0 {
			return
		}
		for _, name := range o.users[uid].Groups {
			group := o.groupByName(name, false)
			if group.ID >= 0 {
				ret <- group
			}
		}
	}()
	return ret
}

func (o *Hackers) Group_All(ctx context.Context, req p.Request_Group_All) <-chan p.Group {
	o.lock.RLock()
	ret := make(chan p.Group)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		for name, _ := range o.groups {
			group := o.groupByName(name, true)
			if group.ID >= 0 {
				ret <- group
			}
		}
	}()
	return ret
}
