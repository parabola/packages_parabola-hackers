// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"context"

	p "git.lukeshu.com/go/libnslcd/nslcd_proto"
)

func (o *Hackers) Config_Get(ctx context.Context, req p.Request_Config_Get) <-chan p.Config {
	o.lock.RLock()
	ret := make(chan p.Config)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		switch req.Key {
		case p.NSLCD_CONFIG_PAM_PASSWORD_PROHIBIT_MESSAGE:
			if o.cfg.Pam_password_prohibit_message != "" {
				ret <- p.Config{Value: o.cfg.Pam_password_prohibit_message}
			}
		}
	}()
	return ret
}
