// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"io/ioutil"
	"os"

	"git.lukeshu.com/go/libgnulinux/getgr"
	yaml "gopkg.in/yaml.v2"
)

func name2gid(name string) int32 {
	gr, err := getgr.ByName(name)
	if gr == nil || err != nil {
		return -1
	} else {
		return int32(gr.Gid)
	}
}

func gid2name(gid int32) (string, bool) {
	gr, err := getgr.ByGid(gid)
	if gr == nil || err != nil {
		return "", false
	} else {
		return gr.Name, true
	}
}

var usersGid = name2gid("users")

func parse_config(filename string) (cfg config, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(contents, &cfg)
	return
}
