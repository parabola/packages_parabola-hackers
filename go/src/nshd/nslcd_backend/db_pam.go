// Copyright 2015-2017 Luke Shumaker <git.lukeshu@sbcglobal>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nslcd_backend

import (
	"context"
	"fmt"
	"os"

	"nshd/nshd_files"
	"nshd/util"

	p "git.lukeshu.com/go/libnslcd/nslcd_proto"
	"git.lukeshu.com/go/libnslcd/nslcd_server"

	"git.lukeshu.com/go/libgnulinux/crypt"
	"git.lukeshu.com/go/libsystemd/sd_daemon"
)

func checkPassword(password string, hash string) bool {
	return crypt.Crypt(password, hash) == hash
}

func hashPassword(newPassword string, oldHash string) string {
	salt := oldHash
	if salt == "!" {
		str, err := util.RandomString(crypt.SaltAlphabet, 8)
		if err != nil {
			sd_daemon.Log.Err("Could not generate a random string")
			str = ""
		}
		// "$6$" is SHA-512; see crypt(3) for others
		salt = "$6$" + str + "$"
	}
	return crypt.Crypt(newPassword, salt)
}

func dirExists(path string) bool {
	stat, err := os.Stat(path)
	if err != nil {
		return false
	}
	return stat.IsDir()
}

func (o *Hackers) canChangePassword(user nshd_files.User, oldpassword string) bool {
	// special hack: if the old password is not
	// set, but the home directory exists, let the
	// user set their password
	if user.Passwd.PwHash == "!" && dirExists(user.Passwd.HomeDir) {
		return true
	}

	return checkPassword(oldpassword, user.Passwd.PwHash)
}

// We don't actually use this for authentication (we let pam_unix.so
// call NSS getspnam(3), which will call our Shadow_ByName()), but
// pam_ldap.so calls this as a pre-flight check for
// pam_sm_chauthtok()/PAM_PwMod().
func (o *Hackers) PAM_Authentication(ctx context.Context, req p.Request_PAM_Authentication) <-chan p.PAM_Authentication {
	o.lock.RLock()
	ret := make(chan p.PAM_Authentication)
	go func() {
		defer o.lock.RUnlock()
		defer close(ret)

		cred, ok := nslcd_server.PeerCredFromContext(ctx)
		if !ok {
			return
		}

		if len(req.UserName) == 0 && len(req.Password) == 0 && cred.Uid == 0 {
			// Being called by root; root can do what root
			// wants.
			ret <- p.PAM_Authentication{
				AuthenticationResult: p.NSLCD_PAM_SUCCESS,
				UserName:             "",
				AuthorizationResult:  p.NSLCD_PAM_SUCCESS,
				AuthorizationError:   "",
			}
			return
		}

		uid := o.name2uid(req.UserName)
		if uid < 0 {
			return
		}
		user := o.users[uid]

		if o.canChangePassword(user, req.Password) {
			// Success
			ret <- p.PAM_Authentication{
				AuthenticationResult: p.NSLCD_PAM_SUCCESS,
				UserName:             user.Passwd.Name,
				AuthorizationResult:  p.NSLCD_PAM_SUCCESS,
				AuthorizationError:   "",
			}
			return
		} else {
			// Failure
			ret <- p.PAM_Authentication{
				AuthenticationResult: p.NSLCD_PAM_AUTH_ERR,
				UserName:             "",
				AuthorizationResult:  p.NSLCD_PAM_AUTH_ERR,
				AuthorizationError:   fmt.Sprintf("password change failed: %s", "Old password did not match"),
			}
			return
		}
	}()
	return ret
}

func (o *Hackers) PAM_PwMod(ctx context.Context, req p.Request_PAM_PwMod) <-chan p.PAM_PwMod {
	ret := make(chan p.PAM_PwMod)
	o.lock.Lock()
	go func() {
		defer o.lock.Unlock()
		defer close(ret)

		cred, ok := nslcd_server.PeerCredFromContext(ctx)
		if !ok {
			return
		}

		uid := o.name2uid(req.UserName)
		if uid < 0 {
			return
		}
		user := o.users[uid]

		// Check the OldPassword
		if req.AsRoot == 1 && cred.Uid == 0 {
			goto update
		}
		if !o.canChangePassword(user, req.OldPassword) {
			ret <- p.PAM_PwMod{
				Result: p.NSLCD_PAM_PERM_DENIED,
				Error:  fmt.Sprintf("password change failed: %s", "Old password did not match"),
			}
			return
		}
	update:
		if len(req.NewPassword) == 0 {
			ret <- p.PAM_PwMod{
				Result: p.NSLCD_PAM_PERM_DENIED,
				Error:  "password cannot be empty",
			}
			return
		}

		// Update the PwHash in memory
		user.Passwd.PwHash = hashPassword(req.NewPassword, user.Passwd.PwHash)
		if len(user.Passwd.PwHash) == 0 {
			sd_daemon.Log.Err("Password hashing failed")
			return
		}

		// Update the PwHash on disk
		passwords := make(map[string]string, len(o.users))
		for _, ouser := range o.users {
			passwords[ouser.Passwd.Name] = ouser.Passwd.PwHash
		}
		passwords[user.Passwd.Name] = user.Passwd.PwHash
		err := nshd_files.SaveAllPasswords(passwords)
		if err != nil {
			sd_daemon.Log.Err(fmt.Sprintf("Writing passwords to disk: %v", err))
			return
		}

		// Ok, we're done, commit the changes
		o.users[uid] = user
		ret <- p.PAM_PwMod{
			Result: p.NSLCD_PAM_SUCCESS,
			Error:  "",
		}
	}()
	return ret
}
