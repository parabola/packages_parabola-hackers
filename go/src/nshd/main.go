// Copyright 2015-2017 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

// Command nshd is an implementation of nslcd that talks to
// hackers.git instead of LDAP.
package main

import (
	"context"
	"os"
	"time"

	"nshd/nshd_files"
	"nshd/nslcd_backend"

	"git.lukeshu.com/go/libnslcd/nslcd_server"
	"git.lukeshu.com/go/libnslcd/nslcd_systemd"
)

func main() {
	backend := &nslcd_backend.Hackers{
		CfgFilename: nshd_files.ConfFile,
	}
	limits := nslcd_server.Limits{
		Timeout: 1 * time.Second,
		RequestMaxSize:/* 1 KiB */ 1024,
	}
	os.Exit(int(nslcd_systemd.Main(backend, limits, context.Background())))
}
