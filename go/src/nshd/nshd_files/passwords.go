// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package nshd_files

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"git.lukeshu.com/go/libgnulinux/crypt"
	"git.lukeshu.com/go/libsystemd/sd_daemon"
)

/* Note that the password hash value should be one of:
   <empty> - no password set, allow login without password
   !       - used to prevent logins
   x       - "valid" encrypted password that does not match any valid password
             often used to indicate that the password is defined elsewhere
   other   - encrypted password, in crypt(3) format */

func LoadAllPasswords() (map[string]string, error) {
	file, err := os.Open(ShadowFile)
	if err != nil {
		return nil, err
	}
	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(string(contents), "\n")
	passwords := make(map[string]string, len(lines))
	for i, line := range lines {
		if line == "" {
			continue
		}
		cols := strings.SplitN(line, ":", 2)
		if len(cols) != 2 {
			sd_daemon.Log.Err(fmt.Sprintf("hackers.git %s:%d: malformed line", ShadowFile, i+1))
			continue
		}
		username := cols[0]
		hash := cols[1]
		if hash != "!" && !crypt.SaltOk(hash) {
			hash = "!"
			sd_daemon.Log.Err(fmt.Sprintf("%s:%d: malformed hash for user: %s", ShadowFile, i+1, username))
		}
		passwords[username] = hash
	}
	return passwords, nil
}

func SaveAllPasswords(passwords map[string]string) error {
	usernames := make([]string, len(passwords))
	i := 0
	for username, _ := range passwords {
		usernames[i] = username
		i++
	}
	sort.Strings(usernames)

	file, err := os.OpenFile(ShadowFile+"-", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}

	for _, username := range usernames {
		fmt.Fprintf(file, "%s:%s\n", username, passwords[username])
	}
	err = file.Sync()
	if err != nil {
		return err
	}
	err = file.Close()
	if err != nil {
		return err
	}

	return os.Rename(ShadowFile+"-", ShadowFile)
}
