// Copyright 2015-2016 Luke Shumaker <lukeshu@sbcglobal.net>.
//
// This is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this manual; if not, see
// <http://www.gnu.org/licenses/>.

package util

import (
	"crypto/rand"
	"math/big"
)

func RandomString(alphabet string, n uint) (str string, err error) {
	var alphabet_len = big.NewInt(int64(len(alphabet)))
	var bigint *big.Int
	_str := make([]byte, n)
	for i := 0; i < len(_str); i++ {
		bigint, err = rand.Int(rand.Reader, alphabet_len)
		if err != nil {
			return
		}
		_str[i] = alphabet[bigint.Int64()]
	}
	str = string(_str[:])
	return
}

func Set2list(set map[string]bool) []string {
	list := make([]string, len(set))
	i := uint(0)
	for item, _ := range set {
		list[i] = item
		i++
	}
	return list
}
