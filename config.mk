ifeq ($(origin topsrcdir),undefined)
topoutdir := $(patsubst %/,%,$(dir $(lastword $(MAKEFILE_LIST))))
topsrcdir := $(topoutdir)

PACKAGE = parabola-hackers
VERSION = 20171221

sysusersdir=$(prefix)/lib/sysusers.d
systemunitdir=$(prefix)/lib/systemd/system
conf_file = $(sysconfdir)/$(PACKAGE).yml
shadow_file = $(sysconfdir)/nshd/shadow
NET ?=
#NET ?= FORCE
user = nshd
CFLAGS += -Wall -Wextra -Werror -pedantic
CC = gcc -std=c99
.LIBPATTERNS = lib%.so lib%.a

endif
