The golang.org/x/sys/unix library can't be updated past commit
ab9e364efd8b52800ff7ee48a9ffba4e0ed78dfb (where I have it right now)
until go commit 93da0b6e66f24c4c307e0df37ceb102a33306174 (currently on
master) makes it in to a release.  It narrowly missed the cut-off to
be in go 1.9; I expect it to be in go 1.10 around February 2018.
