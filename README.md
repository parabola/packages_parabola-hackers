This repository contains tools for working with hackers.git
information.

The most important 4 programs are:

 - `meta-check`: sanity-check hackers.git data
 - `ssh-list-authorized-keys`: configure sshd to use this for
   AuthorizedKeysCommand to have it get SSH keys directly from
   hackers.git
 - `postfix-generate-virtual-map`: generate a virtual map
   for Postfix that provides email aliases for users in hackers.git
 - `pacman-make-keyring` generate a tarball with the pacman-keyring
   files for the users in hackers.git
 - `nshd` implements the nshld protocol of nss-pam-ldapd, but talks to
   hackers.git instead of LDAP.

The others are:

 - `meta-cat`: used by `nshd`
 - `meta-normalize-stdio`: used by `meta-check`
 - `nshd-setuid`: used by `nshd`
 - `nshd-tester`: used to test `nshd`
 - `pgp-list-keyids`: used by `pacman-make-keyring`
 - `uid-map`: used by `pacman-make-keyring`

Each of the programs looks for `parabola-hackers.yml` in he current
directory (except for `meta-normalize-stdio`, which has no
configuration).

# Configuration

The main two things programs at are `yamldir` which tells them where
to find `hackers.git/users`, and `groupgroups` which augments the
`groups` array for each user.  You may override the setting for
`yamldir` by setting the environment variable
`PARABOLA_HACKERS_YAMLDIR`

## pacman-make-keyring

`pacman-make-keyring` also looks at `keyring_cachedir` to see where to
store files that can be cached between versions of the keyring.

## ssh-list-authorized-keys

`ssh-list-authorized-keys` also looks at `ssh_pseudo_users`.
System users (`/etc/passwd`) mentioned in this variable may be SSH'ed
into by hackers.git users who are in a group of the same name.

## nshd

`nshd` also looks at `pam_password_prohibit_message` to decide what to
say when prohibiting a user from being changed via PAM.

# Usage

## meta-check

Just run it, it will report any problems with hackers.git data.

## ssh-list-authorized-keys

Configure `sshd_config:AuthorizedKeysCommand` to be this program.
`sshd` will run it as `ssh-list-authorized-keys ${USERNAME}`

## postfix-generate-virtual-map

	postfix-show-virtual-map > /etc/postfix/virtual-parabola.nu
	postmap hash:/etc/postfix/virtual-parabola.nu

## pacman-make-keyring

	pacman-make-keyring V=$(date -u +%Y%m%d)
	scp parabola-keyring-$(date -u +%Y%m%d).tar.gz repo.parabola.nu:/srv/repo/main/other/parabola-keyring/

or

	cd $(. "$(librelib conf)" && load_files makepkg && echo "$SRCDEST")
	pacman-make-keyring V=$(date -u +%Y%m%d)

In the latter case, it would get uploaded automagically by
`librerelease` when you release a parabola-keyring with the matching
version.

## nshd

Either reboot, or run `systemd-sysusers` to create the nshd user.

Add `ldap` to the `passwd`, `group`, and `shadow` fields in
`/etc/nsswitch.conf`:

	passwd: files ldap
	group: files ldap
	shadow: files ldap

Then enable and start `nshd.socket`:

    systemctl enable --now nshd.socket

----
Copyright (C) 2014, 2016-2017  Luke Shumaker

This documentation file is placed into the public domain.  If that is
not possible in your legal system, I grant you permission to use it in
absolutely every way that I can legally grant to you.
