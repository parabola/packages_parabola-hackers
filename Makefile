# Copyright 2015-2017 Luke Shumaker <lukeshu@sbcglobal.net>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include config.mk
include $(topsrcdir)/build-aux/Makefile.head.mk

unused_gopkg += golang.org/x/sys/plan9
unused_gopkg += golang.org/x/sys/windows
unused_gopkg += git.lukeshu.com/go/libgnulinux/inotify
unused_gopkg += git.lukeshu.com/go/libsystemd/sd_id128
unused_gopkg += git.lukeshu.com/go/libsystemd/sd_messages
_gitfiles.all := $(filter-out $(foreach p,$(unused_gopkg),go/src/$(p)/%),$(_gitfiles.all))

bins_c      = nshd-setuid nshd-tester
bins_go     = nshd
bins_lib    = common.rb
bins_script = $(filter-out %.c %.o %.in $(bins_c) $(bins_go) $(bins_lib),$(notdir $(wildcard $(srcdir)/bin/*)))
bins = $(bins_c) $(bins_go) $(bins_lib) $(bins_script)

.PHONY: FORCE
.SECONDARY:
.DELETE_ON_ERROR:

#
# Go language noise

CGO_CPPFLAGS = $(CPPFLAGS) -U_FORTIFY_SOURCE
CGO_CFLAGS = $(CFLAGS) -O0 -Wno-unused-parameter
CGO_ENABLED = 1

cgo_variables = CGO_ENABLED CGO_CFLAGS CGO_CPPFLAGS CGO_CXXFLAGS CGO_LDFLAGS CC CXX
$(foreach v,$(cgo_variables),$(eval $v ?=))
export $(cgo_variables)

#
# Generate (pre-tarball)

files.src.gen += LICENSE.lgpl-2.1.txt LICENSE.gpl-2.txt LICENSE.apache-2.0.txt

$(srcdir)/LICENSE.lgpl-2.1.txt: $(NET)
	curl https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt > $@
$(srcdir)/LICENSE.gpl-2.txt: $(NET)
	curl https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt > $@
$(srcdir)/LICENSE.apache-2.0.txt: $(NET)
	curl https://www.apache.org/licenses/LICENSE-2.0 > $@
$(srcdir)/LICENSE.wtfpl-2.txt: $(NET)
	curl http://www.wtfpl.net/txt/copying/ > $@

nested.subdirs += $(dir $(filter %/Makefile,$(_gitfiles.all)))

#
# Build (post-tarball)

files.out.all += $(addprefix bin/,$(filter-out $(bins_script),$(bins))) nshd.service nshd.sysusers
files.out.int += go/pkg/ go/bin/ go/bin/*

# Go
$(addprefix %/bin/,$(bins_go)): %/src
	GOPATH=$(abspath $*) go install $(bins_go)
$(outdir)/bin/%: $(outdir)/go/bin/%
	cp -T $< $@
# C
$(outdir)/bin/%: $(srcdir)/bin/%.c $(var.)CC $(var.)CPPFLAGS $(var.)CFLAGS
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o $@ $(filter-out $(var.)%,$^)
# Basic variable substitution
$(outdir)/%: $(srcdir)/%.in
	< $< sed $(foreach v,$(patsubst $(var.)%,%,$(filter $(var.)%,$^)), -e 's|@$v@|$($v)|g' ) > $@
$(outdir)/bin/%: $(srcdir)/bin/%.in
	< $< sed $(foreach v,$(patsubst $(var.)%,%,$(filter $(var.)%,$^)), -e 's|@$v@|$($v)|g' ) > $@

# Dependencies
$(outdir)/bin/nshd-setuid: -ldl
$(outdir)/nshd.service: $(var.)user $(var.)bindir
$(outdir)/nshd.sysusers: $(var.)user
$(outdir)/bin/common.rb: $(var.)conf_file

$(outdir)/go/src/nshd/nshd_files/paths.go: $(var.)bindir $(var.)conf_file $(var.)shadow_file
$(outdir)/go/bin/nshd: $(outdir)/$(files.generate) $(outdir)/go/src/nshd/nshd_files/paths.go
files.out.all += $(outdir)/go/src/nshd/nshd_files/paths.go

go_os_arch = $(word 4,$(shell go version))
go_race_os_arch = $(shell go help build | sed -n '/^\s*-race/,/^\s*-/p'| grep -oE '[a-z0-9]+/[a-z0-9]+')
go_test_flags = $(if $(filter $(go_os_arch),$(go_race_os_arch)),-race)
$(outdir)/check:
	GOPATH=$$PWD/go go test $(go_test_flags) $$(GOPATH=$$PWD/go go list -f '{{.ImportPath}}{{"\n"}}{{join .Deps "\n"}}' $(bins_go) | sort -u | grep -e ^nshd -e '\.[^/]*/')
.PHONY: $(outdir)/check
at.targets += $(outdir)/check


# Install

files.sys.all += $(addprefix $(bindir)/,$(bins)) $(systemunitdir)/nshd.socket $(systemunitdir)/nshd.service $(sysusersdir)/nshd.conf $(conf_file) $(shadow_file)

$(DESTDIR)$(bindir)/%: $(outdir)/bin/%
	$(NORMAL_INSTALL)
	install -TDm755 $< $@
$(DESTDIR)$(systemunitdir)/%.socket: $(outdir)/%.socket
	$(NORMAL_INSTALL)
	install -TDm644 $< $@
$(DESTDIR)$(systemunitdir)/%.service: $(outdir)/%.service
	$(NORMAL_INSTALL)
	install -TDm644 $< $@
$(DESTDIR)$(sysusersdir)/%.conf: $(outdir)/%.sysusers
	$(NORMAL_INSTALL)
	install -TDm644 $< $@

# Specific files
$(DESTDIR)$(bindir)/common.rb: $(srcdir)/bin/common.rb
	$(NORMAL_INSTALL)
	install -TDm644 $< $@
$(DESTDIR)$(conf_file): $(srcdir)/parabola-hackers.yml
	$(NORMAL_INSTALL)
	install -TDm644 $< $@
$(DESTDIR)$(shadow_file): $(var.)user $(DESTDIR)$(sysusersdir)/nshd.conf
	$(NORMAL_INSTALL)
	install -d $(@D)
	touch $@
	$(POST_INSTALL)
	-systemd-sysusers
	-chown $(user):$(user) $(@D) $@

include $(topsrcdir)/build-aux/Makefile.tail.mk
